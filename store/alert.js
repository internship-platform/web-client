import alerts from '~/assets/alerts.json'
export const state = () => ({
  alert: {},
  alerts,
})

export const getters = {
  current: (state) => {
    return state.alert
  },
  extractError: (state) => (err) => {
    if (err && err.graphQLErrors) {
      if (err.graphQLErrors.length > 0) {
        const errObject = err.graphQLErrors[0]

        if (
          errObject.extensions &&
          errObject.extensions.internal &&
          errObject.extensions.internal.error
        ) {
          return errObject.extensions.internal.error.message
        } else {
          return `${err.graphQLErrors[0].message}`
        }
      } else {
        return 'extraction_error'
      }
    } else if (err && err.errors && err.errors.length > 0) {
      return err.errors[0].message
    } else if (typeof err === 'string') {
      return err
    } else {
      return 'unknown_error'
    }
  },

  humanize: (state, getters) => (err) => {
    let info = {}

    const errMsg = getters.extractError(err)

    Object.keys(state.alerts).forEach((item) => {
      if (errMsg.includes(item)) {
        info = {
          ...state.alerts[item],
          key: item,
        }
      }
    })

    if (Object.keys(info).length === 0 && errMsg) {
      info = {
        msg: errMsg,
        type: 'info',
      }
    }
    return info
  },
}
export const mutations = {
  alert(state, data) {
    state.alert = data
  },
}
export const actions = {
  alert({ commit, getters }, { slug, error }) {
    const alert = getters.humanize(error)
    const a = {
      id: Array(3)
        .fill(null)
        .map(() => Math.random().toString(36).substr(2))
        .join(''),
      slug: `app-alert-${slug}`,
      ...alert,
      error,
    }
    console.error(a)
    commit('alert', a)
  },
}
