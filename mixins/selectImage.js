export default {
  data() {
    return {
      image: [],
      previewImage: undefined,
    }
  },
  methods: {
    selectedImage(e) {
      if (e === null) {
        this.previewImage = undefined
        return
      }
      const selectedImage = e

      if (Array.isArray(e)) {
        for (let i = 0; i < e.length; i++) {
          this.createBase64Image(selectedImage[i], e)
        }
      } else this.createBase64Image(selectedImage, e)
    },

    // FUNCTION FOR CREATING BASE 64 TYPE IMAGE FILE TO BE SENT
    createBase64Image(fileObject, selectI) {
      const reader = new FileReader()
      reader.onload = (e) => {
        if (Array.isArray(selectI)) {
          this.image.push(e.target.result)
        } else this.image = e.target.result
      }
      reader.readAsDataURL(fileObject)
      this.previewImage = window.URL.createObjectURL(fileObject)
    },
  },
}
